const express = require('express')
const app = express();
const PORT = process.env.PORT || 3000;

require('./models');

app.use(express.json());

app.get('/', (req, res) => {
  res.status(201).json({
      status:' success',
      message: 'Hello World'
  })
})


app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
})